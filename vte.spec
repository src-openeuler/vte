%define glib2_version 2.26.0
%define pango_version 1.22.0
%define gtk2_version 2.20.0
Name:                vte
Version:             0.28.2
Release:             4
Summary:             A terminal emulator
License:             LGPLv2+
URL:                 http://developer.gnome.org/vte/
Source:              http://download.gnome.org/sources/vte/0.28/%{name}-%{version}.tar.xz
# https://bugzilla.gnome.org/show_bug.cgi?id=663779
Patch0:              vte-alt-meta-confusion.patch
# Python bindings bugfix
# https://bugzilla.redhat.com/show_bug.cgi?id=556200
Patch1:              vte-python-bugfixes.patch
# aarch64 support
Patch3:              vte-aarch64.patch
# Fix control home/control end codes
# https://bugzilla.redhat.com/show_bug.cgi?id=1114074
Patch4:              vte-0.28.2-control.patch
# Fix mc paste
# https://bugzilla.redhat.com/show_bug.cgi?id=1114301
Patch5:              vte-0.28.2-paste-fix.diff
# Backport introspection fixes
# https://bugzilla.redhat.com/show_bug.cgi?id=1256535
Patch6:              vte-0.28.2-introspection-fixes.patch
# Backport "cat bigfile" speedup
# https://bugzilla.gnome.org/show_bug.cgi?id=721944
Patch7:              vte-0.28.2-performance.patch
# Backport shift-mouse grab "hang" fix
# https://bugzilla.gnome.org/show_bug.cgi?id=683730
Patch8:              vte-0.28.2-683730.patch
# Backport extended xterm/urxvt mouse tracking support
# https://bugzilla.gnome.org/show_bug.cgi?id=681329
Patch9:              vte-0.28.2-mouse-tracking.patch
#http://deb.debian.org/debian/pool/main/v/vte/vte_0.28.2-6.debian.tar.xz
#https://bugzilla.gnome.org/show_bug.cgi?id=676090
Patch10:             CVE-2012-2738-1.patch
Patch11:             CVE-2012-2738-2.patch
BuildRequires:       gtk2-devel >= %{gtk2_version} pango-devel >= %{pango_version}
BuildRequires:       glib2-devel >= %{glib2_version} ncurses-devel
BuildRequires:       gettext libXt-devel intltool perl-Carp gobject-introspection-devel
Requires:            systemd
%description
VTE is a terminal emulator widget for use with GTK+ 2.0.

%package devel
Summary:             Files needed for developing applications which use vte
Requires:            %{name} = %{version}-%{release} gtk2-devel ncurses-devel pkgconfig
%description devel
The vte-devel package includes the header files and developer docs
for the vte package.
Install vte-devel if you want to develop programs which will use
vte.

%prep
%setup -q
%patch0 -p1
%patch1 -p1
%patch3 -p1
%patch4 -p1
%patch5 -p1
%patch6 -p2
%patch7 -p1
%patch8 -p1
%patch9 -p1
%patch10 -p1
%patch11 -p1

%build
%configure \
        --enable-shared \
        --enable-static \
        --enable-introspection \
        --with-gtk=2.0 \
        --libexecdir=%{_libdir}/%{name} \
        --without-glX \
        --disable-gtk-doc\
        --disable-python
make %{?_smp_mflags}

%install
make install DESTDIR=$RPM_BUILD_ROOT
rm $RPM_BUILD_ROOT/%{_libdir}/lib%{name}.a
rm $RPM_BUILD_ROOT/%{_libdir}/lib%{name}.la
rm -f $RPM_BUILD_ROOT/%{_libdir}/python*/site-packages/gtk-2.0/*.la
rm -f $RPM_BUILD_ROOT/%{_libdir}/python*/site-packages/gtk-2.0/*.a
%find_lang vte-0.0

%files -f vte-0.0.lang
%doc COPYING HACKING NEWS README
%doc src/iso2022.txt
%doc doc/utmpwtmp.txt doc/boxes.txt doc/openi18n/UTF-8.txt doc/openi18n/wrap.txt
%{_libdir}/*.so.*
%dir %{_libdir}/vte
%attr(2711,root,utmp) %{_libdir}/vte/gnome-pty-helper
%{_datadir}/%{name}
%{_libdir}/girepository-1.0

%files devel
%{_includedir}/*
%{_libdir}/*.so
%{_libdir}/pkgconfig/*
%{_bindir}/%{name}
%{_datadir}/gir-1.0
%doc %{_datadir}/gtk-doc/html/vte-0.0

%changelog
* Thu Apr 14 2022 yaoxin <yaoxin30@h-partners.com> - 0.28.2-4
- Fix CVE-2012-2738

* Wed Aug 18 2021 lingsheng <lingsheng@huawei.com> - 0.28.2-3
- Remove require pygtk2-devel

* Sat Jul 31 2021 zhangtao <zhangtao221@huawei.com> - 0.28.2-2
- disable python2

* Mon Aug 3 2020 fanjiachen <fanjiachen3@huawei.com> - 0.28.2-1
- package init
